package com.company.ctm.util;

import java.util.ArrayList;
import java.util.List;

public class ListUtil {
    public static <E> ArrayList<E> slice(List<E> list, int start, int end) {
        ArrayList<E> newList = new ArrayList<E>();
        for (int i=0; i<list.size(); i++) {
            if (i>=start && i<=end) {
                newList.add(list.get(i));
            }
        }
        return newList;
    }
}
