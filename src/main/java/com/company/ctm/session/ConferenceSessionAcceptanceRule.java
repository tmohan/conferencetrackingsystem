package com.company.ctm.session;

import com.company.ctm.model.Talk;
import java.util.List;

public interface ConferenceSessionAcceptanceRule {    
   List<Talk> accepts(List<Talk> sequence, int start);    
}
