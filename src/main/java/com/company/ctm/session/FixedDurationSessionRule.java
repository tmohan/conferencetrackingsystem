package com.company.ctm.session;

import com.company.ctm.model.Talk;
import com.company.ctm.util.ListUtil;
import java.util.Collections;
import java.util.List;

public class FixedDurationSessionRule implements ConferenceSessionAcceptanceRule {

    private int duration;

    public FixedDurationSessionRule(int duration) {
        this.duration = duration;
    }

    @Override
    public List<Talk> accepts(List<Talk> sequence, int start) {
        List<Talk> emptyList = Collections.EMPTY_LIST;
        if (start >= sequence.size()) {
            return emptyList;
        }

        int total = 0;
        for (int i = start; i < sequence.size(); i++) {
            Talk talk = sequence.get(i);
            total += talk.getDuration();
            if (total == duration) {
                return ListUtil.slice(sequence, start, i);
            } else if (total > duration) {
                return emptyList;
            }
        }
        return emptyList;
    }
}
