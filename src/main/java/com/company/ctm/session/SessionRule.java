package com.company.ctm.session;

public interface SessionRule {
    
    public boolean accepts(int duration);
    
}
