package com.company.ctm.session;

import com.company.ctm.model.Talk;
import com.company.ctm.util.ListUtil;
import java.util.Collections;
import java.util.List;

public class TimeRangeSessionRule implements ConferenceSessionAcceptanceRule {

    private int minimumTime;
    private int maximumTime;
    private static int NOT_FOUND = -1;

    public TimeRangeSessionRule(int minimumTime,
            int maximumTime) {
        this.minimumTime = minimumTime;
        this.maximumTime = maximumTime;
    }

    @Override
    public List<Talk> accepts(List<Talk> sequence, int start) {
        List<Talk> talks = Collections.EMPTY_LIST;

        if (start >= sequence.size()) {
            return talks;
        }

        int total = 0;
        int validIndex = NOT_FOUND;
        for (int i = start; i < sequence.size(); i++) {
            Talk talk = sequence.get(i);
            total += talk.getDuration();
            if (total >= minimumTime && total <= maximumTime) {
                validIndex = i;
            } else if (total >= maximumTime) {
                return ListUtil.slice(sequence, start, validIndex);
            }
        }
        return ListUtil.slice(sequence, start, validIndex);
    }
}
