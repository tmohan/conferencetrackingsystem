package com.company.ctm.session;

public class FixedDurationSession implements SessionRule {
    
    private int duration;
    
    public FixedDurationSession(int duration) {
        this.duration = duration;
    }

    public boolean accepts(int duration) {
        if (this.duration == duration) {
            return true;
        }
        return false;
    }
    
    
    
}
