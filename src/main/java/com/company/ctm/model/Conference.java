package com.company.ctm.model;

import static com.google.common.base.Preconditions.*;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.List;

public class Conference {
    
    private List<Talk> proposals;
    private Track track;
    
    private Conference() {
        proposals = new ArrayList<Talk>();        
    }
    
    
    public static Conference createInstance(List<Talk> proposals) {
        proposals = Lists.newArrayList(proposals);
        checkNotNull(proposals);
        checkArgument(!proposals.isEmpty());
        
        Conference conference = new Conference();
        conference.proposals = proposals;
        return conference;
    }
    
    
    public List<Talk> getProposals() {
        return proposals;
    }
    
    public Track getTrack() {
        return track;
    }
    
    
}
