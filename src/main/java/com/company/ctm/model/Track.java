package com.company.ctm.model;

import com.google.common.base.Objects;
import static com.google.common.base.Preconditions.*;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.List;

public class Track {
    
    private List<Talk> morningSession;
    private List<Talk> eveningSession;
    
    private Track() {
        morningSession = new ArrayList<Talk>();
        eveningSession = new ArrayList<Talk>();        
    }
    
    public static Track createInstance(List<Talk> morningSession, 
            List<Talk> eveningSession) {
        
        morningSession = Lists.newArrayList(morningSession);
        eveningSession = Lists.newArrayList(eveningSession);
        checkNotNull(morningSession);
        checkNotNull(eveningSession);
        checkArgument(!morningSession.isEmpty());
        checkArgument(!eveningSession.isEmpty());
        
        Track track = new Track();
        track.morningSession = morningSession;
        track.eveningSession = eveningSession;
        return track;
    }
    
    public List<Talk> getMorningSession() {
        return morningSession;
    }
    
    public List<Talk> getEveningSession() {
        return eveningSession;
    }
    
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Track)) {
            return false;
        }
        Track track = (Track)other;
        return track.getMorningSession().equals(getMorningSession()) &&
                track.getEveningSession().equals(getEveningSession());           
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getMorningSession(), getEveningSession());
    }
    
    @Override
    public String toString() {        
       return Objects.toStringHelper(this)
               .add("MorningSession=", getMorningSession())
               .add("EveningSession", getEveningSession())
               .toString(); 
    }
    
    public int getTotalSessions() {
        return morningSession.size() + eveningSession.size(); 
    }
}
