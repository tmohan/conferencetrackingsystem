package com.company.ctm.model;

import com.google.common.base.Optional;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class InputFileReader {

    private BufferedReader in = null;
    private boolean open = false;

    public InputFileReader(String fileName) {
        try {
            in = new BufferedReader(new FileReader(fileName));
            open = true;
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public synchronized Optional<String> readNextLine() {
        if (!open) {
            return Optional.absent();  
        }
        try {
            if (in.ready()) {
                String s = in.readLine();
                return Optional.of(s);
            } else {
                closeStream(in);
            }
        } catch (IOException e) {
            System.out.println(e);
            closeStream(in);
        }
        return Optional.absent();        
    }

    void closeStream(BufferedReader in) {
        if (in != null) {
            try {
                in.close();
                open = false;
            } catch (IOException ex) {
                System.out.println(ex);
            }
        }
    }
}
