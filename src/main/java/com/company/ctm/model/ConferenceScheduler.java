package com.company.ctm.model;

import com.google.common.base.Optional;
import com.company.ctm.session.ConferenceSessionAcceptanceRule;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import static com.google.common.collect.Collections2.*;

class ConferenceScheduler {
    
    private ConferenceSessionAcceptanceRule morningSessionRule;
    private ConferenceSessionAcceptanceRule  eveningSessionRule;

    public ConferenceScheduler(ConferenceSessionAcceptanceRule morningSessionRule, 
            ConferenceSessionAcceptanceRule eveningSessionRule) {
        this.morningSessionRule = morningSessionRule;
        this.eveningSessionRule = eveningSessionRule;
    }



    Optional<Track> findTrack(List<Talk> proposals, int start) {
        List<Talk> morningSessions = morningSessionRule.accepts(proposals, start);
        if (morningSessions.isEmpty()) {
            return Optional.absent();
        }

        List<Talk> eveningSessions = eveningSessionRule.accepts(proposals, start + morningSessions.size());
        if (eveningSessions.isEmpty()) {
            return Optional.absent();
        }
        Track track = Track.createInstance(morningSessions, eveningSessions);
        return Optional.of(track);
    }

    Optional<List<Track>> findTracks(List<Talk> proposals) {
        List<Track> tracks = new ArrayList<Track>();
        int i = 0;
        while (i < proposals.size()) {            
                Optional<Track> mightBeTrack = findTrack(proposals, i);
                if (!mightBeTrack.isPresent()) {
                    return Optional.absent(); 
                }
                Track track = mightBeTrack.get();
                i = i + track.getTotalSessions();          
                tracks.add(track);
        }
        return Optional.of(tracks);

    }

    public Optional<List<Track>> getTracks(List<Talk> proposals) {        
        Collection<List<Talk>> permuationsOfProposals = permutations(proposals);

        for (List<Talk> talks : permuationsOfProposals) {
            Optional<List<Track>> mightBeTracks = findTracks(talks);            
            if(mightBeTracks.isPresent()) {
                return mightBeTracks;
            }
        }
        return Optional.absent();
    }
}
