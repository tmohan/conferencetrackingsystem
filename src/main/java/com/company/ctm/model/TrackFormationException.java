package com.company.ctm.model;

public class TrackFormationException extends RuntimeException {

    public TrackFormationException() {
    }

    public TrackFormationException(String message) {
        super(message);
    }

    public TrackFormationException(String message, Throwable cause) {
        super(message, cause);
    }
}
