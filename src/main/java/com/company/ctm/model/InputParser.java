package com.company.ctm.model;

import com.google.common.base.Optional;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InputParser {
    
    private static final int LIGHTNING_DURATION = 5;

    public Optional<Talk> parse(String line) {
        Pattern pattern = Pattern.compile("\\d+");
        Matcher matcher = pattern.matcher(line);
        while (matcher.find()) {
            String eventName = line.substring(0, matcher.start() - 1);
            int duration = Integer.valueOf(matcher.group());
            Talk talk = new Talk(eventName, duration);
            return Optional.of(talk);
        }
        
        pattern = Pattern.compile("lightning");
        matcher = pattern.matcher(line);
        while (matcher.find()) {
            String eventName = line.substring(0, matcher.start() - 1);
            Talk talk = new Talk(eventName, LIGHTNING_DURATION);
            return Optional.of(talk);
        }
        
        return Optional.absent();
    }
    
    public List<Talk> parseInput(String fileName) {
        List<Talk> talks = new ArrayList<Talk>();
        InputFileReader reader = new InputFileReader(fileName);
        Optional<String> line = Optional.absent();
        while ((line = reader.readNextLine()).isPresent()) {
            Optional<Talk> talk = parse(line.get());
            if (talk.isPresent()) {
                talks.add(talk.get());
            }            
        }
        return talks;        
    }
    
}
