package com.company.ctm.model;

import com.google.common.base.Objects;

public class Talk {
    
    private String title;
    private int duration;
    
    public Talk(String title, int duration) {
        this.title = title;
        this.duration = duration;
    }

    public int getDuration() {
        return duration;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "Talk{" + "title=" + title + ", duration=" + duration + '}';
    }
    
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Talk)) {
            return false;
        }
        Talk talk = (Talk) other;
        return talk.getTitle().equals(getTitle()) &&
               talk.getDuration() == getDuration();
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getTitle(), getDuration());
    }
    
    
}
