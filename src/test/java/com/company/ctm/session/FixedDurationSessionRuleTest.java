/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.ctm.session;

import java.util.Collections;
import com.company.ctm.model.Talk;
import com.company.ctm.util.ListUtil;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author xbbkhd7
 */
public class FixedDurationSessionRuleTest {

    private FixedDurationSessionRule rule;

    public FixedDurationSessionRuleTest() {
    }

    @Before
    public void setUp() {
        rule = new FixedDurationSessionRule(60);
    }

    @Test
    public void testAccepts_Valid() {
        List<Talk> proposals = new ArrayList<Talk>();
        Talk talk = new Talk("Writing Fast Tests Against Enterprise Rails", 15);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 45);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 45);
        proposals.add(talk);
        assertEquals(ListUtil.slice(proposals, 0, 1),
                rule.accepts(proposals, 0));

        proposals = new ArrayList<Talk>();
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 60);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 45);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 45);
        proposals.add(talk);
        assertEquals(ListUtil.slice(proposals, 0, 0),
                rule.accepts(proposals, 0));

        proposals = new ArrayList<Talk>();
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 15);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 50);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 60);
        proposals.add(talk);
        assertEquals(ListUtil.slice(proposals, 2, 2),
                rule.accepts(proposals, 2));
    }

    @Test
    public void testAccepts_InValid() {
        List<Talk> proposals = new ArrayList<Talk>();
        Talk talk = new Talk("Writing Fast Tests Against Enterprise Rails", 15);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 50);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 45);
        proposals.add(talk);
        assertEquals(Collections.EMPTY_LIST,
                rule.accepts(proposals, 0));

        proposals = new ArrayList<Talk>();
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 15);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 50);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 60);
        proposals.add(talk);
        assertEquals(Collections.EMPTY_LIST,
                rule.accepts(proposals, 1));


        proposals = new ArrayList<Talk>();
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 50);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 50);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 45);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 20);
        proposals.add(talk);
        assertEquals(Collections.EMPTY_LIST,
                rule.accepts(proposals, 2));

        proposals = new ArrayList<Talk>();
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 50);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 50);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 45);
        proposals.add(talk);
        assertEquals(Collections.EMPTY_LIST,
                rule.accepts(proposals, 3));

        proposals = new ArrayList<Talk>();
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 50);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 50);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 45);
        proposals.add(talk);
        assertEquals(Collections.EMPTY_LIST,
                rule.accepts(proposals, 2));

        proposals = new ArrayList<Talk>();
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 5);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 15);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 10);
        proposals.add(talk);
        assertEquals(Collections.EMPTY_LIST,
                rule.accepts(proposals, 0));
    }
}
