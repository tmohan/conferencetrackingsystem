/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.ctm.session;

import java.util.Collections;
import com.company.ctm.model.Talk;
import com.company.ctm.util.ListUtil;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author xbbkhd7
 */
public class TimeRangeSessionRuleTest {

    private TimeRangeSessionRule rule;

    public TimeRangeSessionRuleTest() {
    }

    @Before
    public void setUp() {
        rule = new TimeRangeSessionRule(60, 90);
    }

    @Test
    public void testAccepts_ValidSequence() {
        List<Talk> proposals = new ArrayList<Talk>();
        Talk talk = new Talk("Writing Fast Tests Against Enterprise Rails", 30);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 30);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 45);
        proposals.add(talk);
        assertEquals(ListUtil.slice(proposals, 0, 1),
                rule.accepts(proposals, 0));

        proposals = new ArrayList<Talk>();
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 60);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 15);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 45);
        proposals.add(talk);
        assertEquals(ListUtil.slice(proposals, 0, 1),
                rule.accepts(proposals, 0));

        proposals = new ArrayList<Talk>();
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 60);
        proposals.add(talk);
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 90);
        proposals.add(talk);
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 60);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 15);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 45);
        proposals.add(talk);
        assertEquals(ListUtil.slice(proposals, 2, 3),
                rule.accepts(proposals, 2));

        proposals = new ArrayList<Talk>();
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 60);
        proposals.add(talk);
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 90);
        proposals.add(talk);
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 90);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 15);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 45);
        proposals.add(talk);
        assertEquals(ListUtil.slice(proposals, 2, 2),
                rule.accepts(proposals, 2));


        proposals = new ArrayList<Talk>();
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 60);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 15);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 15);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 15);
        proposals.add(talk);
        assertEquals(ListUtil.slice(proposals, 0, 2),
                rule.accepts(proposals, 0));

    }

    @Test
    public void testAccepts_InValidSequence() {
        List<Talk> proposals = new ArrayList<Talk>();
        Talk talk = new Talk("Writing Fast Tests Against Enterprise Rails", 30);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 30);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 45);
        proposals.add(talk);
        assertEquals(Collections.EMPTY_LIST,
                rule.accepts(proposals, 3));

        proposals = new ArrayList<Talk>();
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 30);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 100);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 45);
        proposals.add(talk);
        assertEquals(Collections.EMPTY_LIST,
                rule.accepts(proposals, 1));

        proposals = new ArrayList<Talk>();
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 15);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 90);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 45);
        proposals.add(talk);
        assertEquals(Collections.EMPTY_LIST,
                rule.accepts(proposals, 0));

        proposals = new ArrayList<Talk>();
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 5);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 5);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 5);
        proposals.add(talk);
        assertEquals(Collections.EMPTY_LIST,
                rule.accepts(proposals, 0));
    }
}
