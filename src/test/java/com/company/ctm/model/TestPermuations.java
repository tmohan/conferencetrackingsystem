/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.ctm.model;

import com.google.common.math.BigIntegerMath;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static com.google.common.collect.Collections2.*;
import static org.junit.Assert.*;

/**
 *
 * @author xbbkhd7
 */
public class TestPermuations {
    

    @Test
    public void testPermuations() {
        List<Talk> proposals = new ArrayList<Talk>();
        Talk talk1 = new Talk("Writing Fast Tests Against Enterprise Rails", 60);
        proposals.add(talk1);
        Talk talk2 = new Talk("Overdoing it in Python", 45);
        proposals.add(talk2);
        Talk talk3 = new Talk("Overdoing it in Python", 45);
        proposals.add(talk3);
        
        Collection<List<Talk>> permuations = permutations(proposals);
        assertTrue(BigIntegerMath.factorial(proposals.size()).intValue()
                == permuations.size());
        
        for (List<Talk> talks : permuations) {
            assertTrue(talks.size() == proposals.size());
            assertTrue(talks.contains(talk1));
            assertTrue(talks.contains(talk2));            
            assertTrue(talks.contains(talk3));           
        }
    }
    
}
