/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.ctm.model;

import java.util.List;
import com.google.common.base.Optional;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author xbbkhd7
 */
public class InputParserTest {
    
    public InputParserTest() {
    }

    @Test
    public void testParse() {
        InputParser inputParser = new InputParser();
        Optional<Talk> talk = inputParser.parse("Writing Fast Tests Against Enterprise Rails 60min");
        assertEquals(new Talk("Writing Fast Tests Against Enterprise Rails", 60), talk.get());

        talk = inputParser.parse("Rails for Python Developers lightning");
        assertEquals(new Talk("Rails for Python Developers", 5), talk.get());
    }
    
    @Test
    public void testParseInput() {
        InputParser inputParser = new InputParser();
        List<Talk> proposals = inputParser.parseInput("input.txt");
        System.out.println(proposals);
        
    }
}
