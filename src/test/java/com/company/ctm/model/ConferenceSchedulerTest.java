package com.company.ctm.model;

import com.google.common.base.Optional;
import com.company.ctm.session.ConferenceSessionAcceptanceRule;
import org.junit.Before;
import com.company.ctm.session.FixedDurationSessionRule;
import com.company.ctm.session.TimeRangeSessionRule;
import com.company.ctm.util.ListUtil;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class ConferenceSchedulerTest {
    
    private ConferenceScheduler conferenceScheduler;
    
    @Before
    public void setUp() {
        ConferenceSessionAcceptanceRule morningSessionRule =  new FixedDurationSessionRule(60);
        ConferenceSessionAcceptanceRule eveningSessionRule =  new TimeRangeSessionRule(60, 90);

        conferenceScheduler = new ConferenceScheduler(morningSessionRule, eveningSessionRule);
    }

 
    @Test
    public void testFindTrack_ValidSequence() {
        List<Talk> proposals = new ArrayList<Talk>();
        Talk talk = new Talk("Writing Fast Tests Against Enterprise Rails", 60);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 90);
        proposals.add(talk);

        assertEquals(
                Track.createInstance(
                ListUtil.slice(proposals, 0, 0), ListUtil.slice(proposals, 1, 1)),
                conferenceScheduler.findTrack(proposals, 0).get());

        proposals = new ArrayList<Talk>();
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 15);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 40);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 5);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 30);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 30);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 5);
        proposals.add(talk);
        assertEquals(
                Track.createInstance(
                ListUtil.slice(proposals, 0, 2), ListUtil.slice(proposals, 3, 5)),
                conferenceScheduler.findTrack(proposals, 0).get());


        proposals = new ArrayList<Talk>();
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 60);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 60);
        proposals.add(talk);

        assertEquals(
                Track.createInstance(
                ListUtil.slice(proposals, 0, 0), ListUtil.slice(proposals, 1, 1)),
                conferenceScheduler.findTrack(proposals, 0).get());
    }

    @Test
    public void testFindTrack_InValidSequence() {
        List<Talk> proposals = new ArrayList<Talk>();
        Talk talk = new Talk("Writing Fast Tests Against Enterprise Rails", 60);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 90);
        proposals.add(talk);

        assertEquals(
                Track.createInstance(
                ListUtil.slice(proposals, 0, 0), ListUtil.slice(proposals, 1, 1)),
                conferenceScheduler.findTrack(proposals, 0).get());

        proposals = new ArrayList<Talk>();
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 15);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 40);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 5);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 30);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 30);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 5);
        proposals.add(talk);
        assertEquals(
                Track.createInstance(
                ListUtil.slice(proposals, 0, 2), ListUtil.slice(proposals, 3, 5)),
                conferenceScheduler.findTrack(proposals, 0).get());


        proposals = new ArrayList<Talk>();
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 60);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 60);
        proposals.add(talk);

        assertEquals(
                Track.createInstance(
                ListUtil.slice(proposals, 0, 0), ListUtil.slice(proposals, 1, 1)),
                conferenceScheduler.findTrack(proposals, 0).get());
    }

    @Test
    public void testFindTracks_ValidSequence() {
        List<Talk> proposals = new ArrayList<Talk>();
        Talk talk = new Talk("Writing Fast Tests Against Enterprise Rails", 60);
        proposals.add(talk);
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 90);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 60);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 60);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 30);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 60);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 90);
        proposals.add(talk);
        
        List<Talk> morningSessions = new ArrayList<Talk>();
        List<Talk> eveningSessions = new ArrayList<Talk>();
        morningSessions.add(proposals.get(0));
        eveningSessions.add(proposals.get(1));
        Track track = Track.createInstance(morningSessions, eveningSessions);
        ArrayList<Track> tracks = new ArrayList<Track>();
        tracks.add(track);
        
        morningSessions = new ArrayList<Talk>();
        eveningSessions = new ArrayList<Talk>();
        morningSessions.add(proposals.get(2));
        eveningSessions.add(proposals.get(3));
        eveningSessions.add(proposals.get(4));
        track = Track.createInstance(morningSessions, eveningSessions);
        tracks.add(track);
        
        morningSessions = new ArrayList<Talk>();
        eveningSessions = new ArrayList<Talk>();
        morningSessions.add(proposals.get(5));
        eveningSessions.add(proposals.get(6));
        track = Track.createInstance(morningSessions, eveningSessions);
        tracks.add(track);

        assertEquals(tracks, conferenceScheduler.findTracks(proposals).get());

        proposals = new ArrayList<Talk>();
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 15);
        proposals.add(talk);
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 45);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 15);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 15);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 30);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 45);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 15);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 30);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 30);
        proposals.add(talk);
        
        
        morningSessions = new ArrayList<Talk>();
        eveningSessions = new ArrayList<Talk>();
        morningSessions.add(proposals.get(0));
        morningSessions.add(proposals.get(1));
        eveningSessions.add(proposals.get(2));
        eveningSessions.add(proposals.get(3));
        eveningSessions.add(proposals.get(4));
        track = Track.createInstance(morningSessions, eveningSessions);
        tracks = new ArrayList<Track>();
        tracks.add(track);
        
        morningSessions = new ArrayList<Talk>();
        eveningSessions = new ArrayList<Talk>();
        morningSessions.add(proposals.get(5));
        morningSessions.add(proposals.get(6));
        eveningSessions.add(proposals.get(7));
        eveningSessions.add(proposals.get(8));
        track = Track.createInstance(morningSessions, eveningSessions);
        tracks.add(track);
        
        assertEquals(tracks, conferenceScheduler.findTracks(proposals).get());

    }

    @Test
    public void testFindTracks_InValidSequence() {
        List<Talk> proposals = new ArrayList<Talk>();
        Talk talk = new Talk("Writing Fast Tests Against Enterprise Rails", 65);
        proposals.add(talk);
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 90);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 60);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 60);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 30);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 60);
        proposals.add(talk);
        talk = new Talk("Overdoing it in Python", 90);
        proposals.add(talk);
        assertFalse(conferenceScheduler.findTracks(proposals).isPresent());
        
        
    }
    
    public void testGetTracks_ValidSequenceLarge() {
        ConferenceSessionAcceptanceRule morningSessionRule =  new FixedDurationSessionRule(180);
        ConferenceSessionAcceptanceRule eveningSessionRule =  new TimeRangeSessionRule(180, 240);

        ConferenceScheduler conferenceScheduler = new ConferenceScheduler(morningSessionRule, eveningSessionRule);
        List<Talk> proposals = new ArrayList<Talk>();
        Talk talk = new Talk("Writing Fast Tests Against Enterprise Rails", 60);
        proposals.add(talk);        
        talk = new Talk("Overdoing it in Python", 45);
        proposals.add(talk);
        talk = new Talk("Lua for the Masses", 30);
        proposals.add(talk);
        
        talk = new Talk("Ruby Errors from Mismatched Gem Versions", 45);
        proposals.add(talk);
        talk = new Talk("Common Ruby Errors", 45);
        proposals.add(talk);
        talk = new Talk("Rails for Python Developers", 5);
        proposals.add(talk);
        talk = new Talk("Communicating Over Distance", 60);
        proposals.add(talk);
        talk = new Talk("Accounting-Driven Development", 45);
        proposals.add(talk);
        talk = new Talk("Woah", 30);
        proposals.add(talk);
        talk = new Talk("Sit Down and Write", 30);
        proposals.add(talk);
        talk = new Talk("Pair Programming vs Noise", 45);
        proposals.add(talk);
        talk = new Talk("Rails Magic", 60);
        proposals.add(talk);
        talk = new Talk("Ruby on Rails: Why We Should Move On", 60);
        proposals.add(talk);
        talk = new Talk("Clojure Ate Scala (on my project)", 45);
        proposals.add(talk);
        talk = new Talk("Programming in the Boondocks of Seattle", 30);
        proposals.add(talk);
        talk = new Talk("Ruby vs. Clojure for Back-End Development", 30);
        proposals.add(talk);
        talk = new Talk("Ruby on Rails Legacy App Maintenance", 60);
        proposals.add(talk);
        talk = new Talk("A World Without HackerNews", 30);
        proposals.add(talk);
        talk = new Talk("Sit Down and Write", 30);
        proposals.add(talk);
        talk = new Talk("User Interface CSS in Rails Apps", 30);
        proposals.add(talk);
        
        List<Talk> morningSessions = new ArrayList<Talk>();
        List<Talk> eveningSessions = new ArrayList<Talk>();
        morningSessions.add(proposals.get(0));
        eveningSessions.add(proposals.get(1));
        Track track = Track.createInstance(morningSessions, eveningSessions);
        ArrayList<Track> tracks = new ArrayList<Track>();
        tracks.add(track);
        
        morningSessions = new ArrayList<Talk>();
        eveningSessions = new ArrayList<Talk>();
        morningSessions.add(proposals.get(2));
        eveningSessions.add(proposals.get(3));
        eveningSessions.add(proposals.get(4));
        track = Track.createInstance(morningSessions, eveningSessions);
        tracks.add(track);
        
        morningSessions = new ArrayList<Talk>();
        eveningSessions = new ArrayList<Talk>();
        morningSessions.add(proposals.get(5));
        eveningSessions.add(proposals.get(6));
        track = Track.createInstance(morningSessions, eveningSessions);
        tracks.add(track);        
        
       System.out.println("conferenceScheduler" + conferenceScheduler.getTracks(proposals).get());

       //assertEquals(tracks, conferenceScheduler.getTracks(proposals).get());
    }
    
    @Test
    public void testGetTracks_ValidSequence() {   
        List<Talk> proposals = new ArrayList<Talk>();
        Talk talk = new Talk("Writing Fast Tests Against Enterprise Rails", 90);
        proposals.add(talk);        
        talk = new Talk("Overdoing it in Python", 60);
        proposals.add(talk);

        List<Talk> morningSessions = new ArrayList<Talk>();
        List<Talk> eveningSessions = new ArrayList<Talk>();
        morningSessions.add(proposals.get(1));
        eveningSessions.add(proposals.get(0));
        Track track = Track.createInstance(morningSessions, eveningSessions);
        List<Track> tracks = new ArrayList<Track>();
        tracks.add(track);
        assertEquals(tracks, conferenceScheduler.getTracks(proposals).get());
        
        
        proposals = new ArrayList<Talk>();
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 60);
        proposals.add(talk);        
        talk = new Talk("Overdoing it in Python", 60);
        proposals.add(talk);        
        talk = new Talk("Rails for Python Developers", 5);
        proposals.add(talk);

        morningSessions = new ArrayList<Talk>();
        eveningSessions = new ArrayList<Talk>();
        morningSessions.add(proposals.get(0));
        eveningSessions.add(proposals.get(1));
        eveningSessions.add(proposals.get(2));
        track = Track.createInstance(morningSessions, eveningSessions);
        tracks = new ArrayList<Track>();
        tracks.add(track);
        assertEquals(tracks, conferenceScheduler.getTracks(proposals).get());
    }
    
    @Test
    public void testGetTracks_InValidSequence() { //TODO  
        List<Talk> proposals = new ArrayList<Talk>();
        Talk talk = new Talk("Writing Fast Tests Against Enterprise Rails", 90);
        proposals.add(talk);        
        talk = new Talk("Overdoing it in Python", 60);
        proposals.add(talk);

        List<Talk> morningSessions = new ArrayList<Talk>();
        List<Talk> eveningSessions = new ArrayList<Talk>();
        morningSessions.add(proposals.get(1));
        eveningSessions.add(proposals.get(0));
        Track track = Track.createInstance(morningSessions, eveningSessions);
        List<Track> tracks = new ArrayList<Track>();
        tracks.add(track);
        assertEquals(tracks, conferenceScheduler.getTracks(proposals).get());
        
        
        proposals = new ArrayList<Talk>();
        talk = new Talk("Writing Fast Tests Against Enterprise Rails", 60);
        proposals.add(talk);        
        talk = new Talk("Overdoing it in Python", 60);
        proposals.add(talk);        
        talk = new Talk("Rails for Python Developers", 5);
        proposals.add(talk);

        morningSessions = new ArrayList<Talk>();
        eveningSessions = new ArrayList<Talk>();
        morningSessions.add(proposals.get(0));
        eveningSessions.add(proposals.get(1));
        eveningSessions.add(proposals.get(2));
        track = Track.createInstance(morningSessions, eveningSessions);
        tracks = new ArrayList<Track>();
        tracks.add(track);
        assertEquals(tracks, conferenceScheduler.getTracks(proposals).get());
    }
    
}
