/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.ctm.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class ConferenceTest {

    @Test
    public void testCreateInstance() {
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateInstanceEmptyProposal() {
        Conference.createInstance(Collections.EMPTY_LIST);
    }

    @Test(expected = NullPointerException.class)
    public void testCreateInstanceNullProposal() {
        Conference.createInstance(null);
    }

    @Test
    public void testGetProposals() {
        List<Talk> proposals = new ArrayList<Talk>();
        Talk talk =  new Talk("Writing Fast Tests Against Enterprise Rails", 60);
        proposals.add(talk);        
        Conference conference = Conference.createInstance(proposals);
        
        assertEquals(proposals, conference.getProposals());
    }
}
