package com.company.ctm.model;

import org.junit.Test;
import static org.junit.Assert.*;

public class TalkTest {
    
    @Test
    public void testTalk() {
        Talk talk =  new Talk("Writing Fast Tests Against Enterprise Rails", 60);
        assertEquals("Writing Fast Tests Against Enterprise Rails", talk.getTitle());
        assertEquals(60, talk.getDuration());       
    }
}
